var args = arguments[0] || {};

if(args.title){
	$.pageTitle.text=args.title;
}

function closeWindow(){
	if(Alloy.Globals.focusedWindow!=null){
		if(Alloy.Globals.focusedWindow){
			Alloy.Globals.focusedWindow.close();
		}
	}
}
